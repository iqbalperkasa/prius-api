const db = require('./db')

module.exports = {
  isUserMemberOf: async (userId, team) => {
    return (await db.query(`select count(*) n from members join teams on teams._id = members._teams_id where members.users_id like ? and teams.name like ?`, [userId, team]))[0]['n'] === 1
  },

  isUsernameFound: async (username) => {
    return (await db.query(`select count(*) n from users where username = ?`, [username]))[0]['n'] === 1
  },

  isTeamFound: async (name) => {
    return (await db.query(`select count(*) n from teams where name = ?`, [name]))[0]['n'] === 1
  },

  isPrFound: async (repo, pr) => {
    return (await db.query(`select count(*) n from lists where repo = ? and href like ?`, [repo, `%pull/${pr}`]))[0]['n'] === 1
  },

  isPrTeamOf: async (team, repo, pr) => {
    return (await db.query(`select count(*) n from lists join teams on lists._teams_id = teams._id where teams.name = ? and lists.repo = ? and lists.href like ?`, [team, repo, `%pull/${pr}%`]))[0]['n'] === 1
  }
}
