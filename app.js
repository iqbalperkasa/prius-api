require('dotenv').config()

const express = require('express')
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const logger = require('pino')()
const db = require('./db')
const helper = require('./helper')
const { createHash, createHmac } = require('crypto')
const nanoid = require('nanoid')
const helmet = require('helmet')

const API_PREFIX = process.env.API_PREFIX

const app = express()

app.use(helmet())
app.use(cookieParser())

const jsonParser = bodyParser.json()
const formParser = bodyParser.urlencoded({ extended: true })

const allowedOrigins = [
  'http://localhost:1234',
  'https://prius.serveo.net',
  'https://prius.iqbalperkasa.com',
]

app.use(cors({
  credentials: true,
  origin: function (origin, callback) {
    if (!origin) return callback(null, true)

    if (allowedOrigins.indexOf(origin) === -1) {
      var msg = 'The CORS policy for this site does not allow access from the specified Origin.'
      return callback(new Error(msg), false);
    }

    return callback(null, true);
  }
}))

app.get('/healthz', ({ res }) => {
  res.send('OK')
})

app.post(API_PREFIX + '/user_sessions', [jsonParser], (req, res) => {
  let payload = {}

  const user = req.body.user
  payload.user = user

  const browserFp = nanoid(32)
  payload.browser_fp = browserFp

  let accessToken = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '7d' })
  accessToken = saltToken(accessToken)

  res.cookie('browser_fp', browserFp, { expires: new Date(new Date().getTime() + 86409000 * 365), httpOnly: true })
  res.json({
    success: true,
    access_token: accessToken
  })
})

app.get(API_PREFIX + '/teams', [verifyToken], async (req, res) => {
  const userId = req.token.user.id

  // TODO
  // team members counter, pr counter

  let teams

  try {
    teams = (await db.query(`select teams.name name from members join teams on members._teams_id = teams._id where members.users_id = ?`, [userId]))
  } catch (err) {
    console.log(err)
  }

  res.json({
    success: true,
    teams
  })
})

app.get(API_PREFIX + '/lists/:team', [jsonParser, verifyToken], async (req, res) => {
  const team = req.params.team
  const userId = req.token.user.id

  if (!await helper.isTeamFound(team)) return res.status(404).json({
    success: false,
    message: 'team_not_found'
  })

  if (!await helper.isUserMemberOf(userId, team)) return res.status(403).json({
    success: false,
    message: 'not_allowed'
  })

  let lists

  try {
    lists = (await db.query(`select repo, title, username, href, lists.created_at created_at from lists join teams on lists._teams_id = teams._id join users on lists._users_id = users._id where teams.name = ? and lists.status = 'show' order by lists.created_at asc`, [team]))
  } catch (err) {
    console.log(err)
    return logger.error(err)
  }

  res.json({
    success: true,
    lists
  })
})

app.get(API_PREFIX + '/lists/:team/pr/:pr', [jsonParser, verifyToken], async (req, res) => {
  const team = req.params.team
  const pr = req.params.pr

  console.log(team, pr)

  return res.json({
    success: true
  })
})

app.delete(API_PREFIX + '/lists/:team', [jsonParser, verifyToken], async (req, res) => {
  const team = req.params.team
  const userId = req.token.user.id
  const repo = req.query.repo
  const pr = req.query.pr

  if (!repo) return res.status(400).json({
    success: false,
    message: 'repo_name_missing'
  })

  if (!pr) return res.status(400).json({
    success: false,
    message: 'pr_number_missing'
  })

  if (!await helper.isTeamFound(team)) return res.status(404).json({
    success: false,
    message: 'team_not_found'
  })

  if (!await helper.isUserMemberOf(userId, team)) return res.status(403).json({
    success: false,
    message: 'user_not_allowed'
  })

  if (!await helper.isPrFound(repo, pr)) return res.status(400).json({
    success: false,
    message: 'pr_not_found'
  })

  if (!await helper.isPrTeamOf(team, repo, pr)) return res.status(403).json({
    success: false,
    message: 'team_not_allowed'
  })

  try {
    await db.query(`update lists set status = 'hidden' where repo = ? and href like ?`, [repo, '%pull/' + pr])
  } catch (err) {
    console.log(err)
  }

  res.json({
    success: true
  })
})

app.post(API_PREFIX + '/check_token', [verifyToken], (req, res) => {
  res.json({
    success: true
  })
})

app.use(({ res }) => {
  return res.status(404).json({
    success: false,
    message: 'endpoint_not_found'
  })
})

function saltToken (token) {
  const character = process.env.JWT_SALT_CHARACTER
  const position = parseInt(process.env.JWT_SALT_POSITION)

  return [token.slice(0, position), character, token.slice(position)].join('')
}

function deSaltToken (token) {
  const position = parseInt(process.env.JWT_SALT_POSITION)

  return token.slice(0, position) + token.slice(position + 1)
}

function verifyTelegramLogin (req, res, next) {
  const user = req.body.user || req.token.user

  if (!user) return res.status(401).json({
    success: false,
    message: 'login_with_telegram_needed'
  })

  const hash = user.hash
  const secretKey = createHash('sha256').update(process.env.BOT_TOKEN).digest()

  const data = Object.keys(user)
    .sort()
    .filter(keys => keys !== 'hash')
    .map(keys => `${keys}=${user[keys]}`)
    .join('\n')
  const dataHash = createHmac('sha256', secretKey).update(data).digest('hex')

  if (hash !== dataHash) return res.status(403).json({
    success: false,
    message: 'login_invalid'
  })

  next()
}

function verifyToken (req, res, next) {
  const bearer = req.headers['authorization']
  const browserFp = req.cookies.browser_fp

  if (bearer) {
    let accessToken = bearer.split(' ')[1]
    accessToken = deSaltToken(accessToken)

    try {
      const token = jwt.verify(accessToken, process.env.JWT_SECRET)

      if (process.env.NODE_ENV === 'production') {
        if (browserFp !== token.browser_fp) return res.status(401).json({
          success: false,
          message: 'invalid_access_token'
        })
      }

      req.token = token

      return next()
    } catch (err) {
      // if (accessTokenBypass(req)) return next()

      res.status(401).json({
        success: false,
        message: 'invalid_access_token'
      })
    }
  } else {
    // if (accessTokenBypass(req)) return next()

    res.status(403).json({
      success: false,
      message: 'access_token_needed'
    })
  }
}

module.exports = app
